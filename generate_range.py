def gen_range(start, stop, step):
    while start <= stop:
        yield start
        start += step

itr_range = gen_range(10, 30, 3)
print(next(itr_range))
print(next(itr_range))
print(next(itr_range))
print(next(itr_range))
